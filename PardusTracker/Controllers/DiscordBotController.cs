﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PardusTracker.Domain;
using PardusTracker.Models;

namespace PardusTracker.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DiscordBotController : ControllerBase
    {
        private readonly IDiscordBotService _discordBotService;

        public DiscordBotController(IDiscordBotService discordBotService)
        {
            _discordBotService = discordBotService;
        }

        [HttpGet]
        public IActionResult Echo()
        {
            return Ok("hi");
        }

        [HttpGet]
        [Route("StarbasePopulationUpdate")]
        public IActionResult GetPopulationUpdate([FromQuery] Universe universe)
        {
            return Ok(_discordBotService.GetStarbasePopulationUpdateIfExists(universe));
            //var res = _discordBotService.GetStarbasePopulationUpdateIfExists(universe);
            //var json = JsonConvert.SerializeObject(res);
            //return Ok(json);
        }
    }
}
