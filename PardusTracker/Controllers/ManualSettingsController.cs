﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PardusTracker.Domain;
using PardusTracker.Models;

namespace PardusTracker.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ManualSettingsController : ControllerBase
    {
        private readonly IManualSettingsService _manualSettingsService;

        public ManualSettingsController(IManualSettingsService manualSettingsService)
        {
            _manualSettingsService = manualSettingsService;
        }

        [HttpGet]
        public IActionResult Echo()
        {
            return Ok("hi");
        }

        [HttpGet]
        [Route("StarbaseSetting")]
        public IActionResult GetPopulationUpdate(Universe universe, string name, int? min, int? max)
        {
            return Ok(_manualSettingsService.SetStarbaseSetting(universe, name, min, max));
        }
    }
}
