﻿using PardusTracker.Domain;
using Microsoft.AspNetCore.Mvc;
using PardusTracker.Models;

namespace PardusTracker.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoggerScriptController : ControllerBase
    {
        private readonly ILoggerScriptService _service;

        public LoggerScriptController(ILoggerScriptService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Echo()
        {
            return Ok("hi");
        }

        [HttpPost]
        public void Post([FromBody] IncomingUpdate information)
        {
            _service.SaveUpdate(information);
        }
    }
}
