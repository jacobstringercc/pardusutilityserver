﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PardusTracker.Migrations
{
    public partial class AddCachedStarbaseNameToStarbaseObject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Updates");

            migrationBuilder.AddColumn<string>(
                name: "StarbaseName",
                table: "Updates",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LatestStarbaseName",
                table: "Starbases",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StarbaseName",
                table: "Updates");

            migrationBuilder.DropColumn(
                name: "LatestStarbaseName",
                table: "Starbases");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Updates",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);
        }
    }
}
