﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PardusTracker.Migrations
{
    public partial class AddedPardusSquadIDToSquad : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PardusSquadID",
                table: "Squads",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PardusSquadID",
                table: "Squads");
        }
    }
}
