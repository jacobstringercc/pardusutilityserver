﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PardusTracker.Migrations
{
    public partial class SecondMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Commodity_Update_UpdateID",
                table: "Commodity");

            migrationBuilder.DropForeignKey(
                name: "FK_Equipment_Update_UpdateID",
                table: "Equipment");

            migrationBuilder.DropForeignKey(
                name: "FK_PCStarbasePopulationIndividualItem_PCStarbasePopulationUpdates_PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItem");

            migrationBuilder.DropForeignKey(
                name: "FK_Ship_Update_UpdateID",
                table: "Ship");

            migrationBuilder.DropForeignKey(
                name: "FK_Squad_Update_UpdateID",
                table: "Squad");

            migrationBuilder.DropForeignKey(
                name: "FK_StarbaseBuilding_Update_UpdateID",
                table: "StarbaseBuilding");

            migrationBuilder.DropForeignKey(
                name: "FK_Update_Starbases_StarbaseID",
                table: "Update");

            migrationBuilder.DropForeignKey(
                name: "FK_Update_Users_UserID",
                table: "Update");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Update",
                table: "Update");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StarbaseBuilding",
                table: "StarbaseBuilding");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Squad",
                table: "Squad");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PCStarbasePopulationIndividualItem",
                table: "PCStarbasePopulationIndividualItem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Commodity",
                table: "Commodity");

            migrationBuilder.RenameTable(
                name: "Update",
                newName: "Updates");

            migrationBuilder.RenameTable(
                name: "StarbaseBuilding",
                newName: "StarbaseBuildings");

            migrationBuilder.RenameTable(
                name: "Squad",
                newName: "Squads");

            migrationBuilder.RenameTable(
                name: "PCStarbasePopulationIndividualItem",
                newName: "PCStarbasePopulationIndividualItems");

            migrationBuilder.RenameTable(
                name: "Commodity",
                newName: "Commodities");

            migrationBuilder.RenameIndex(
                name: "IX_Update_UserID",
                table: "Updates",
                newName: "IX_Updates_UserID");

            migrationBuilder.RenameIndex(
                name: "IX_Update_StarbaseID",
                table: "Updates",
                newName: "IX_Updates_StarbaseID");

            migrationBuilder.RenameIndex(
                name: "IX_StarbaseBuilding_UpdateID",
                table: "StarbaseBuildings",
                newName: "IX_StarbaseBuildings_UpdateID");

            migrationBuilder.RenameIndex(
                name: "IX_Squad_UpdateID",
                table: "Squads",
                newName: "IX_Squads_UpdateID");

            migrationBuilder.RenameIndex(
                name: "IX_PCStarbasePopulationIndividualItem_PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItems",
                newName: "IX_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdateId");

            migrationBuilder.RenameIndex(
                name: "IX_Commodity_UpdateID",
                table: "Commodities",
                newName: "IX_Commodities_UpdateID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Updates",
                table: "Updates",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StarbaseBuildings",
                table: "StarbaseBuildings",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Squads",
                table: "Squads",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PCStarbasePopulationIndividualItems",
                table: "PCStarbasePopulationIndividualItems",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Commodities",
                table: "Commodities",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Commodities_Updates_UpdateID",
                table: "Commodities",
                column: "UpdateID",
                principalTable: "Updates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Equipment_Updates_UpdateID",
                table: "Equipment",
                column: "UpdateID",
                principalTable: "Updates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdates_PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItems",
                column: "PCStarbasePopulationUpdateId",
                principalTable: "PCStarbasePopulationUpdates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Ship_Updates_UpdateID",
                table: "Ship",
                column: "UpdateID",
                principalTable: "Updates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Squads_Updates_UpdateID",
                table: "Squads",
                column: "UpdateID",
                principalTable: "Updates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StarbaseBuildings_Updates_UpdateID",
                table: "StarbaseBuildings",
                column: "UpdateID",
                principalTable: "Updates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Updates_Starbases_StarbaseID",
                table: "Updates",
                column: "StarbaseID",
                principalTable: "Starbases",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Updates_Users_UserID",
                table: "Updates",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Commodities_Updates_UpdateID",
                table: "Commodities");

            migrationBuilder.DropForeignKey(
                name: "FK_Equipment_Updates_UpdateID",
                table: "Equipment");

            migrationBuilder.DropForeignKey(
                name: "FK_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdates_PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItems");

            migrationBuilder.DropForeignKey(
                name: "FK_Ship_Updates_UpdateID",
                table: "Ship");

            migrationBuilder.DropForeignKey(
                name: "FK_Squads_Updates_UpdateID",
                table: "Squads");

            migrationBuilder.DropForeignKey(
                name: "FK_StarbaseBuildings_Updates_UpdateID",
                table: "StarbaseBuildings");

            migrationBuilder.DropForeignKey(
                name: "FK_Updates_Starbases_StarbaseID",
                table: "Updates");

            migrationBuilder.DropForeignKey(
                name: "FK_Updates_Users_UserID",
                table: "Updates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Updates",
                table: "Updates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StarbaseBuildings",
                table: "StarbaseBuildings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Squads",
                table: "Squads");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PCStarbasePopulationIndividualItems",
                table: "PCStarbasePopulationIndividualItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Commodities",
                table: "Commodities");

            migrationBuilder.RenameTable(
                name: "Updates",
                newName: "Update");

            migrationBuilder.RenameTable(
                name: "StarbaseBuildings",
                newName: "StarbaseBuilding");

            migrationBuilder.RenameTable(
                name: "Squads",
                newName: "Squad");

            migrationBuilder.RenameTable(
                name: "PCStarbasePopulationIndividualItems",
                newName: "PCStarbasePopulationIndividualItem");

            migrationBuilder.RenameTable(
                name: "Commodities",
                newName: "Commodity");

            migrationBuilder.RenameIndex(
                name: "IX_Updates_UserID",
                table: "Update",
                newName: "IX_Update_UserID");

            migrationBuilder.RenameIndex(
                name: "IX_Updates_StarbaseID",
                table: "Update",
                newName: "IX_Update_StarbaseID");

            migrationBuilder.RenameIndex(
                name: "IX_StarbaseBuildings_UpdateID",
                table: "StarbaseBuilding",
                newName: "IX_StarbaseBuilding_UpdateID");

            migrationBuilder.RenameIndex(
                name: "IX_Squads_UpdateID",
                table: "Squad",
                newName: "IX_Squad_UpdateID");

            migrationBuilder.RenameIndex(
                name: "IX_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItem",
                newName: "IX_PCStarbasePopulationIndividualItem_PCStarbasePopulationUpdateId");

            migrationBuilder.RenameIndex(
                name: "IX_Commodities_UpdateID",
                table: "Commodity",
                newName: "IX_Commodity_UpdateID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Update",
                table: "Update",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StarbaseBuilding",
                table: "StarbaseBuilding",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Squad",
                table: "Squad",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PCStarbasePopulationIndividualItem",
                table: "PCStarbasePopulationIndividualItem",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Commodity",
                table: "Commodity",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Commodity_Update_UpdateID",
                table: "Commodity",
                column: "UpdateID",
                principalTable: "Update",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Equipment_Update_UpdateID",
                table: "Equipment",
                column: "UpdateID",
                principalTable: "Update",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PCStarbasePopulationIndividualItem_PCStarbasePopulationUpdates_PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItem",
                column: "PCStarbasePopulationUpdateId",
                principalTable: "PCStarbasePopulationUpdates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Ship_Update_UpdateID",
                table: "Ship",
                column: "UpdateID",
                principalTable: "Update",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Squad_Update_UpdateID",
                table: "Squad",
                column: "UpdateID",
                principalTable: "Update",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StarbaseBuilding_Update_UpdateID",
                table: "StarbaseBuilding",
                column: "UpdateID",
                principalTable: "Update",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Update_Starbases_StarbaseID",
                table: "Update",
                column: "StarbaseID",
                principalTable: "Starbases",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Update_Users_UserID",
                table: "Update",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
