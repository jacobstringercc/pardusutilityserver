﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PardusTracker.Migrations
{
    public partial class RemoveUpdateFlagOnUpdatePopulations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UpdateAlreadyServed",
                table: "PCStarbasePopulationUpdates");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "UpdateAlreadyServed",
                table: "PCStarbasePopulationUpdates",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
