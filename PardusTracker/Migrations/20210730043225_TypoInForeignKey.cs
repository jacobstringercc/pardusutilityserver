﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PardusTracker.Migrations
{
    public partial class TypoInForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdates_PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItems");

            migrationBuilder.RenameColumn(
                name: "PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItems",
                newName: "PCStarbasePopulationUpdateID");

            migrationBuilder.RenameIndex(
                name: "IX_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItems",
                newName: "IX_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdateID");

            migrationBuilder.AddForeignKey(
                name: "FK_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdates_PCStarbasePopulationUpdateID",
                table: "PCStarbasePopulationIndividualItems",
                column: "PCStarbasePopulationUpdateID",
                principalTable: "PCStarbasePopulationUpdates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdates_PCStarbasePopulationUpdateID",
                table: "PCStarbasePopulationIndividualItems");

            migrationBuilder.RenameColumn(
                name: "PCStarbasePopulationUpdateID",
                table: "PCStarbasePopulationIndividualItems",
                newName: "PCStarbasePopulationUpdateId");

            migrationBuilder.RenameIndex(
                name: "IX_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdateID",
                table: "PCStarbasePopulationIndividualItems",
                newName: "IX_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdateId");

            migrationBuilder.AddForeignKey(
                name: "FK_PCStarbasePopulationIndividualItems_PCStarbasePopulationUpdates_PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItems",
                column: "PCStarbasePopulationUpdateId",
                principalTable: "PCStarbasePopulationUpdates",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
