﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PardusTracker.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PCStarbasePopulationUpdates",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTimeOffset>(nullable: false),
                    Universe = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PCStarbasePopulationUpdates", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Starbases",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Universe = table.Column<int>(nullable: false),
                    Sector = table.Column<string>(maxLength: 30, nullable: true),
                    X = table.Column<int>(nullable: false),
                    Y = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Starbases", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PardusID = table.Column<int>(nullable: false),
                    Alias = table.Column<string>(maxLength: 30, nullable: true),
                    Universe = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "PCStarbasePopulationIndividualItem",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PCStarbasePopulationUpdateId = table.Column<int>(nullable: false),
                    StarbaseName = table.Column<string>(maxLength: 50, nullable: true),
                    Population = table.Column<int>(nullable: false),
                    Cluster = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PCStarbasePopulationIndividualItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PCStarbasePopulationIndividualItem_PCStarbasePopulationUpdates_PCStarbasePopulationUpdateId",
                        column: x => x.PCStarbasePopulationUpdateId,
                        principalTable: "PCStarbasePopulationUpdates",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Update",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Owner = table.Column<string>(maxLength: 30, nullable: true),
                    UpdateTime = table.Column<DateTimeOffset>(nullable: false),
                    Population = table.Column<int>(nullable: true),
                    PopulationIsExact = table.Column<bool>(nullable: true),
                    Credits = table.Column<int>(nullable: true),
                    FreeSpace = table.Column<int>(nullable: true),
                    StarbaseID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Update", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Update_Starbases_StarbaseID",
                        column: x => x.StarbaseID,
                        principalTable: "Starbases",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Update_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Commodity",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UpdateID = table.Column<int>(nullable: false),
                    Name = table.Column<int>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    Min = table.Column<int>(nullable: false),
                    Max = table.Column<int>(nullable: false),
                    BuyPrice = table.Column<int>(nullable: false),
                    SellPrice = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commodity", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Commodity_Update_UpdateID",
                        column: x => x.UpdateID,
                        principalTable: "Update",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Equipment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UpdateID = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    Price = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipment", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Equipment_Update_UpdateID",
                        column: x => x.UpdateID,
                        principalTable: "Update",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ship",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UpdateID = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    Price = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ship", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Ship_Update_UpdateID",
                        column: x => x.UpdateID,
                        principalTable: "Update",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Squad",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UpdateID = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Strength = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Squad", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Squad_Update_UpdateID",
                        column: x => x.UpdateID,
                        principalTable: "Update",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StarbaseBuilding",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UpdateID = table.Column<int>(nullable: false),
                    Location = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StarbaseBuilding", x => x.ID);
                    table.ForeignKey(
                        name: "FK_StarbaseBuilding_Update_UpdateID",
                        column: x => x.UpdateID,
                        principalTable: "Update",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Commodity_UpdateID",
                table: "Commodity",
                column: "UpdateID");

            migrationBuilder.CreateIndex(
                name: "IX_Equipment_UpdateID",
                table: "Equipment",
                column: "UpdateID");

            migrationBuilder.CreateIndex(
                name: "IX_PCStarbasePopulationIndividualItem_PCStarbasePopulationUpdateId",
                table: "PCStarbasePopulationIndividualItem",
                column: "PCStarbasePopulationUpdateId");

            migrationBuilder.CreateIndex(
                name: "IX_Ship_UpdateID",
                table: "Ship",
                column: "UpdateID");

            migrationBuilder.CreateIndex(
                name: "IX_Squad_UpdateID",
                table: "Squad",
                column: "UpdateID");

            migrationBuilder.CreateIndex(
                name: "IX_StarbaseBuilding_UpdateID",
                table: "StarbaseBuilding",
                column: "UpdateID");

            migrationBuilder.CreateIndex(
                name: "IX_Update_StarbaseID",
                table: "Update",
                column: "StarbaseID");

            migrationBuilder.CreateIndex(
                name: "IX_Update_UserID",
                table: "Update",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Commodity");

            migrationBuilder.DropTable(
                name: "Equipment");

            migrationBuilder.DropTable(
                name: "PCStarbasePopulationIndividualItem");

            migrationBuilder.DropTable(
                name: "Ship");

            migrationBuilder.DropTable(
                name: "Squad");

            migrationBuilder.DropTable(
                name: "StarbaseBuilding");

            migrationBuilder.DropTable(
                name: "PCStarbasePopulationUpdates");

            migrationBuilder.DropTable(
                name: "Update");

            migrationBuilder.DropTable(
                name: "Starbases");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
