﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PardusTracker.Migrations
{
    public partial class StarbaseMinMaxPops : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaximumPop",
                table: "Starbases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MinimumPop",
                table: "Starbases",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaximumPop",
                table: "Starbases");

            migrationBuilder.DropColumn(
                name: "MinimumPop",
                table: "Starbases");
        }
    }
}
