﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PardusTracker.Migrations
{
    public partial class FlagIfPCSBUpdateAlreadyReported : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "UpdateAlreadyServed",
                table: "PCStarbasePopulationUpdates",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UpdateAlreadyServed",
                table: "PCStarbasePopulationUpdates");
        }
    }
}
