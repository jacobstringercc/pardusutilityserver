﻿using PardusTracker.Models;

namespace PardusTracker.Data
{
    public interface IPCStarbasePopulationRepository
    {
        void SaveUpdate(IncomingUpdate update);
        PCStarbasePopulationUpdateDto GetUpdateIfExists(Universe universe);
    }
}