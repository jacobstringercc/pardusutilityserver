﻿using PardusTracker.Models;

namespace PardusTracker.Data
{
    public interface IStarbaseRepository
    {
        Starbase GetStarbase(Universe universe, string sector, int xCoord, int yCoord);
        Starbase GetStarbase(Universe universe, string sector, string name);
        Starbase GetStarbase(Universe universe, string name);
        void SaveUpdate(IncomingUpdate update);
        void SaveChanges();
    }
}