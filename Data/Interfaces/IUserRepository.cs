﻿using PardusTracker.Models;

namespace PardusTracker.Data
{
    public interface IUserRepository
    {
        User GetExistingOrCreateNewUser(Universe universe, int pardusUserId);
    }
}