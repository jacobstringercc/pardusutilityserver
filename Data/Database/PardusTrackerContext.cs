﻿using Microsoft.EntityFrameworkCore;
using PardusTracker.Models;

namespace PardusTracker.Data
{
    public class PardusTrackerContext : DbContext
    {
        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<Starbase> Starbases { get; set; }
        public virtual DbSet<Update> Updates { get; set; }
        public virtual DbSet<Commodity> Commodities { get; set; }
        public virtual DbSet<Squad> Squads { get; set; }
        public virtual DbSet<Equipment> Equipment { get; set; }
        public virtual DbSet<StarbaseBuilding> StarbaseBuildings { get; set; }

        public virtual DbSet<PCStarbasePopulationUpdate> PCStarbasePopulationUpdates { get; set; }
        public virtual DbSet<PCStarbasePopulationIndividualItem> PCStarbasePopulationIndividualItems { get; set; }

        public PardusTrackerContext(DbContextOptions<PardusTrackerContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
