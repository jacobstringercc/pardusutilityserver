﻿using PardusTracker.Models;
using System.Linq;

namespace PardusTracker.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly PardusTrackerContext _ctx;

        public UserRepository(PardusTrackerContext ctx)
        {
            _ctx = ctx;
        }

        private User GetUser(Universe universe, int pardusUserId) => _ctx.Users.Where(user => user.PardusID == pardusUserId && user.Universe == universe).FirstOrDefault();

        public User GetExistingOrCreateNewUser(Universe universe, int id)
        {
            User user = GetUser(universe, id);

            if (user == null)
            {
                user = new User
                {
                    PardusID = id,
                    Universe = universe
                };
                _ctx.Users.Add(user);
            }

            return user;
        }
    }
}
