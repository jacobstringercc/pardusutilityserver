﻿using PardusTracker.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PardusTracker.Data
{
    public class StarbaseRepository : IStarbaseRepository
    {
        private readonly PardusTrackerContext _ctx;
        private readonly IUserRepository _userRepository;

        public StarbaseRepository(PardusTrackerContext ctx, IUserRepository userRepository)
        {
            _ctx = ctx;
            _userRepository = userRepository;
        }

        public void SaveChanges() => _ctx.SaveChanges();

        public void SaveUpdate(IncomingUpdate update)
        {
            if (update.Starbase.Name == null && update.Starbase.Coords == null)
            {
                return;
            }

            Starbase existingSb = GetExistingOrCreateNewStarbase(update);
            User existingUser = _userRepository.GetExistingOrCreateNewUser(update.Universe, update.User);

            Update newUpdateObject = new Update
            {
                StarbaseName = update.Starbase.Name,
                Owner = update.Starbase.Owner,
                UpdateTime = DateTimeOffset.UtcNow,

                UserID = existingUser.ID,
                User = existingUser,

                StarbaseID = existingSb.ID,
                Starbase = existingSb,

                Population = update.Starbase.Population,
                PopulationIsExact = update.Starbase.PopulationIsExact,
                FreeSpace = update.Starbase.FreeSpace,
                Credits = update.Starbase.Credits
            };

            newUpdateObject.Buildings = MapStarbaseBuildingInfo(update, newUpdateObject);
            newUpdateObject.Commodities = MapCommodityInfo(update, newUpdateObject);
            newUpdateObject.Ships = MapShipInfo(update, newUpdateObject);
            newUpdateObject.Equipment = MapEquipmentInfo(update, newUpdateObject);
            newUpdateObject.Squads = MapSquadInfo(update, newUpdateObject);

            _ctx.Updates.Add(newUpdateObject);
            _ctx.SaveChanges();

            if (update.Starbase.Buildings.Count > 0)
            {
                UpdateMinMaxPops(existingSb);
                _ctx.SaveChanges();
            }
        }

        private void UpdateMinMaxPops(Starbase sb)
        {
            var currentBuildings = GetCurrentBuildings(sb, sb.MinimumPopEdited);
            if (currentBuildings.Count < 16) // A full building update has not been done since the last check
            {
                return;
            }

            int highestRing = currentBuildings
                .Select(pair => pair.Value == StarbaseBuildingType.Empty || pair.Value == StarbaseBuildingType.NotAvailable ? 1 : StarbaseBuildingLocationMapper.NumRingLocation(pair.Key))
                .Max();

            sb.MinimumPop = highestRing == 1 ? 500 :
                highestRing == 2 ? 5000 :
                highestRing == 3 ? 15000 : 30000;
            sb.MinimumPopEdited = DateTimeOffset.UtcNow;

            _ctx.SaveChanges();
        }

        private Dictionary<StarbaseBuildingLocation, StarbaseBuildingType> GetCurrentBuildings(Starbase sb, DateTimeOffset? reportedSince)
        {
            reportedSince = null;
            Dictionary<StarbaseBuildingLocation, StarbaseBuildingType> res = new Dictionary<StarbaseBuildingLocation, StarbaseBuildingType>();
            var buildingsToConsider = reportedSince == null
                ? sb.Updates.SelectMany(update => update.Buildings)
                : sb.Updates.Where(update => update.UpdateTime > reportedSince).SelectMany(update => update.Buildings);
            
            foreach (var building in buildingsToConsider.OrderByDescending(building => building.ID))
            {
                if (!res.ContainsKey(building.Location))
                {
                    res[building.Location] = building.Type;
                }

                if (res.Count == 16)
                {
                    break;
                }
            }
            return res;
        }

        public Starbase GetStarbase(Universe universe, string sector, int xCoord, int yCoord) =>
            _ctx.Starbases.Where(starbase => starbase.Universe == universe && starbase.Sector == sector && starbase.X == xCoord && starbase.Y == yCoord).FirstOrDefault();

        public Starbase GetStarbase(Universe universe, string sector, string name) =>
            _ctx.Starbases.Where(starbase => starbase.Universe == universe && starbase.Sector == sector && starbase.LatestStarbaseName == name).FirstOrDefault();

        public Starbase GetStarbase(Universe universe, string name) =>
            _ctx.Starbases.Where(starbase => starbase.Universe == universe && starbase.LatestStarbaseName == name).FirstOrDefault();

        private bool TryGetStarbase(Universe universe, string sector, int xCoord, int yCoord, out Starbase sb)
        {
            sb = GetStarbase(universe, sector, xCoord, yCoord);
            return sb != null;
        }

        // To be used when coordinates are not available (if a SB renames and this gets hit rather than the above, the update won't be relevant)
        private bool TryGetStarbase(Universe universe, string sector, string name, out Starbase sb)
        {
            sb = GetStarbase(universe, sector, name);
            return sb != null;
        }

        private Starbase GetExistingOrCreateNewStarbase(IncomingUpdate update)
        {
            // Set defaults if necessary for ease of logic, not able to collide with other SB positions
            int defaultCoordinate = -16;
            Coordinates coordinatesUsed = update.Starbase.Coords ?? new Coordinates { X = defaultCoordinate, Y = defaultCoordinate };

            Starbase starbaseObject;

            if (TryGetStarbase(update.Universe, update.Starbase.Sector, coordinatesUsed.X, coordinatesUsed.Y, out starbaseObject) || 
                TryGetStarbase(update.Universe, update.Starbase.Sector, update.Starbase.Name, out starbaseObject))
            {
                if (coordinatesUsed.X != defaultCoordinate && coordinatesUsed.Y != defaultCoordinate)
                {
                    starbaseObject.LatestStarbaseName = update.Starbase.Name;
                }

                // Update default coordinates from previous update to correct coordinates
                if (starbaseObject.X == defaultCoordinate && coordinatesUsed.X != defaultCoordinate)
                {
                    starbaseObject.X = coordinatesUsed.X;
                    starbaseObject.Y = coordinatesUsed.Y;
                    RemoveStarbasesWithin15Tiles(starbaseObject);
                }
            }
            else
            {
                starbaseObject = new Starbase
                {
                    X = coordinatesUsed.X,
                    Y = coordinatesUsed.Y,
                    Sector = update.Starbase.Sector,
                    Universe = update.Universe,
                    IsActive = true
                };

                // We don't want to remove other SBs that could also be using default coordinates
                if (starbaseObject.X != defaultCoordinate)
                {
                    RemoveStarbasesWithin15Tiles(starbaseObject);
                }
                _ctx.Starbases.Add(starbaseObject);
            }

            return starbaseObject;
        }

        private ICollection<StarbaseBuilding> MapStarbaseBuildingInfo(IncomingUpdate update, Update updateObj)
        {
            return update.Starbase.Buildings.Select(building => new StarbaseBuilding
            {
                Location = StarbaseBuildingLocationMapper.ToBuildingLocation(building.Key),
                Type = StarbaseBuildingTypeMapper.ToBuildingType(building.Value),
                Update = updateObj
            }).ToArray();
        }

        private ICollection<Commodity> MapCommodityInfo(IncomingUpdate update, Update updateObj)
        {
            return update.Starbase.Commodities.Select(commodity =>
            {
                return new Commodity
                {
                    Update = updateObj,
                    Name = CommodityTypeMapper.FromString(commodity.Key),
                    Amount = commodity.Value.Amount,
                    Min = commodity.Value.Min,
                    Max = commodity.Value.Max,
                    BuyPrice = commodity.Value.Buy,
                    SellPrice = commodity.Value.Sell,
                };
            }).ToArray();
        }

        private ICollection<Squad> MapSquadInfo(IncomingUpdate update, Update updateObj)
        {
            return update.Starbase.Squads.Select(squad =>
            {
                SquadType type;
                if (Enum.TryParse(squad.Value.Type, out type))
                {
                    return new Squad
                    {
                        Update = updateObj,
                        Type = type,
                        Strength = squad.Value.Size,
                        PardusSquadID = int.Parse(squad.Key)
                    };
                }
                else
                {
                    throw new Exception($"{squad.Key} is not able to be parsed to SquadType");
                }
            }).ToArray();
        }

        private ICollection<Ship> MapShipInfo(IncomingUpdate update, Update updateObj)
        {
            return update.Starbase.Ships.Select(ship =>
            {
                return new Ship
                {
                    Update = updateObj,
                    Type = ShipTypeMapper.FromString(ship.Key),
                    Amount = ship.Value
                };
            }).ToArray();
        }

        private ICollection<Equipment> MapEquipmentInfo(IncomingUpdate update, Update updateObj)
        {
            return update.Starbase.Equipment.Select(equipmentGroup =>
                equipmentGroup.Value.Select(equipmentItem =>
                {
                    return new Equipment
                    {
                        Update = updateObj,
                        Type = EquipmentTypeMapper.FromString(equipmentItem.Key),
                        Amount = equipmentItem.Value
                    };
                })
            ).SelectMany(x => x).ToArray();
        }

        private void RemoveStarbasesWithin15Tiles(Starbase newSb)
        {
            foreach (var oldSb in _ctx.Starbases.Where(oldSb => oldSb.IsActive && Math.Abs(oldSb.X - newSb.X) < 15 || Math.Abs(oldSb.Y - newSb.Y) < 15)) 
            {
                oldSb.IsActive = false;
            }
            _ctx.SaveChanges();
        }
    }
}
