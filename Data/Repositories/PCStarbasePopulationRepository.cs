﻿using Common;
using Microsoft.EntityFrameworkCore;
using PardusTracker.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace PardusTracker.Data
{
    public class PCStarbasePopulationRepository : IPCStarbasePopulationRepository
    {
        private readonly PardusTrackerContext _ctx;

        public PCStarbasePopulationRepository(PardusTrackerContext ctx)
        {
            _ctx = ctx;
        }

        public void SaveUpdate(IncomingUpdate update)
        {
            if (update?.PardusCluster?.PFC == null
                || update.PardusCluster.PFC.Count == 0)
            {
                return;
            }

            DateTimeOffset createdDate = DateTimeOffset.ParseExact(update.PardusCluster.Updated, "ddd MMM d H:mm:ss GMT yyyy", CultureInfo.InvariantCulture);
            if (PopulationUpdateForThisSBTickAlreadyExists(update.Universe, createdDate))
            {
                return; 
            }

            PCStarbasePopulationUpdate newUpdateObject = new PCStarbasePopulationUpdate
            {
                CreatedDate = createdDate,
                Universe = update.Universe
            };

            newUpdateObject.Items = new IEnumerable<PCStarbasePopulationIndividualItem>[] {
                update.PardusCluster.PEC.Select(starbaseAndSize => MapToStarbaseItem(newUpdateObject, Cluster.PEC, starbaseAndSize)),
                update.PardusCluster.PFC.Select(starbaseAndSize => MapToStarbaseItem(newUpdateObject, Cluster.PFC, starbaseAndSize)),
                update.PardusCluster.PUC.Select(starbaseAndSize => MapToStarbaseItem(newUpdateObject, Cluster.PUC, starbaseAndSize)),
            }
            .SelectMany(x => x)
            .ToArray();

            _ctx.PCStarbasePopulationUpdates.Add(newUpdateObject);

            // TODO update buildings on SB if a ring has been lost

            _ctx.SaveChanges();
        }

        private bool PopulationUpdateForThisSBTickAlreadyExists(Universe universe, DateTimeOffset updated)
        {
            var mostRecentUpdate = _ctx.PCStarbasePopulationUpdates.Where(x => x.Universe == universe).OrderByDescending(x => x.ID).FirstOrDefault();
            return mostRecentUpdate != null && mostRecentUpdate.CreatedDate.isSameStarbaseTick(updated);
        }

        private PCStarbasePopulationIndividualItem MapToStarbaseItem(PCStarbasePopulationUpdate parent, Cluster cluster, KeyValuePair<string, int> starbasesAndSizes)
        {
            return new PCStarbasePopulationIndividualItem
            {
                PCStarbasePopulationUpdate = parent,
                Cluster = cluster,
                StarbaseName = starbasesAndSizes.Key,
                Population = starbasesAndSizes.Value
            };
        }

        public PCStarbasePopulationUpdateDto GetUpdateIfExists(Universe universe)
        {
            var mostRecentUpdates = _ctx.PCStarbasePopulationUpdates
                .Where(x => x.Universe == universe)
                .OrderByDescending(x => x.ID)
                .Take(2)
                .Include(x => x.Items)
                .ToArray();

            if (mostRecentUpdates.Length != 2
                || !mostRecentUpdates[0].CreatedDate.isNextStarbaseTick(mostRecentUpdates[1].CreatedDate))
            {
                return null;
            }

            PCStarbasePopulationUpdateDto update = new PCStarbasePopulationUpdateDto
            {
                PEC = new Dictionary<string, int[]>(),
                PFC = new Dictionary<string, int[]>(),
                PUC = new Dictionary<string, int[]>()
            };

            foreach (var item in mostRecentUpdates[1].Items)
            {
                switch (item.Cluster)
                {
                    case Cluster.PEC:
                        update.PEC[item.StarbaseName] = new int[2];
                        update.PEC[item.StarbaseName][0] = item.Population;
                        break;
                    case Cluster.PFC:
                        update.PFC[item.StarbaseName] = new int[2];
                        update.PFC[item.StarbaseName][0] = item.Population;
                        break;
                    case Cluster.PUC:
                        update.PUC[item.StarbaseName] = new int[2];
                        update.PUC[item.StarbaseName][0] = item.Population;
                        break;
                    default:
                        throw new NotImplementedException("Cluster not supported");
                }
            }

            foreach (var item in mostRecentUpdates[0].Items)
            {
                switch (item.Cluster)
                {
                    case Cluster.PEC:
                        if (update.PEC.ContainsKey(item.StarbaseName))
                            update.PEC[item.StarbaseName][1] = item.Population;
                        break;
                    case Cluster.PFC:
                        if (update.PFC.ContainsKey(item.StarbaseName))
                            update.PFC[item.StarbaseName][1] = item.Population;
                        break;
                    case Cluster.PUC:
                        if (update.PUC.ContainsKey(item.StarbaseName))
                            update.PUC[item.StarbaseName][1] = item.Population;
                        break;
                    default:
                        throw new NotImplementedException("Cluster not supported");
                }
            }

            return update;
        }
    }
}
