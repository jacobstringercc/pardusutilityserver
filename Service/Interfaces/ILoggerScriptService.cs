﻿using PardusTracker.Models;

namespace PardusTracker.Domain
{
    public interface ILoggerScriptService
    {
        void SaveUpdate(IncomingUpdate information);
    }
}