﻿using PardusTracker.Models;

namespace PardusTracker.Domain
{
    public interface IDiscordBotService
    {
        PCStarbasePopulationUpdateDto GetStarbasePopulationUpdateIfExists(Universe universe);
    }
}