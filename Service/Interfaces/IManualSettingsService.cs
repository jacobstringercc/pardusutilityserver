﻿using PardusTracker.Models;

namespace PardusTracker.Domain
{
    public interface IManualSettingsService
    {
        bool SetStarbaseSetting(Universe universe, string name, int? min, int? max);
    }
}