﻿using PardusTracker.Data;
using PardusTracker.Models;

namespace PardusTracker.Domain
{
    public class DiscordBotService : IDiscordBotService
    {
        private readonly IPCStarbasePopulationRepository _starbasePopulationRepository;

        public DiscordBotService(IPCStarbasePopulationRepository starbasePopulationRepository)
        {
            _starbasePopulationRepository = starbasePopulationRepository;
        }

        public PCStarbasePopulationUpdateDto GetStarbasePopulationUpdateIfExists(Universe universe)
        {
            return _starbasePopulationRepository.GetUpdateIfExists(universe);
        }
    }
}
