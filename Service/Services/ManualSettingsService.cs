﻿using PardusTracker.Data;
using PardusTracker.Models;
using System;

namespace PardusTracker.Domain
{
    public class ManualSettingsService : IManualSettingsService
    {
        private readonly IStarbaseRepository _starbaseRepository;

        public ManualSettingsService(IStarbaseRepository starbaseRepository)
        {
            _starbaseRepository = starbaseRepository;
        }

        public bool SetStarbaseSetting(Universe universe, string name, int? min, int? max)
        {
            Starbase sb = _starbaseRepository.GetStarbase(universe, name);
            bool res = sb != null;

            if (res)
            {
                if (min != null)
                {
                    sb.MinimumPop = min;
                    sb.MinimumPopEdited = DateTimeOffset.UtcNow;
                }
                if (max != null)
                {
                    sb.MaximumPop = max;
                }
                _starbaseRepository.SaveChanges();
            }

            return res;
        }
    }
}
