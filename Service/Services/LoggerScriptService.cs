﻿using PardusTracker.Data;
using PardusTracker.Models;

namespace PardusTracker.Domain
{
    public class LoggerScriptService : ILoggerScriptService
    {
        private readonly IStarbaseRepository _starbaseRepository;
        private readonly IPCStarbasePopulationRepository _pcStarbasePopulationRepository;

        public LoggerScriptService(IStarbaseRepository starbaseRepository, IPCStarbasePopulationRepository pcStarbasePopulationRepository)
        {
            _starbaseRepository = starbaseRepository;
            _pcStarbasePopulationRepository = pcStarbasePopulationRepository;
        }

        public void SaveUpdate(IncomingUpdate information)
        {
            _starbaseRepository.SaveUpdate(information);
            _pcStarbasePopulationRepository.SaveUpdate(information);
        }
    }
}
