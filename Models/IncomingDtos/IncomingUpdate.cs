﻿using System.Collections.Generic;

namespace PardusTracker.Models
{
    public class IncomingUpdate
    {
        public int User { get; set; }
        public Universe Universe { get; set; }
        public StarbaseDto Starbase { get; set; }
        public PardusClusterInformation PardusCluster { get; set; }
    }

    public class PardusClusterInformation
    {
        public string Updated { get; set; }
        public Dictionary<string, int> PEC { get; set; }
        public Dictionary<string, int> PFC { get; set; }
        public Dictionary<string, int> PUC { get; set; }
    }

    public class StarbaseDto
    {
        public string Name { get; set; }
        public string Owner { get; set; }
        public int Population { get; set; }
        public bool PopulationIsExact { get; set; }
        public string Sector { get; set; }
        public Coordinates Coords { get; set; }
        public int Credits { get; set; }
        public int FreeSpace { get; set; }
        public Dictionary<string, string> Buildings { get; set; }
        public Dictionary<string, CommodityDto> Commodities { get; set; }
        public Dictionary<string, Dictionary<string, int>> Equipment { get; set; }
        public Dictionary<string, SquadDto> Squads { get; set; }
        public Dictionary<string, int> Ships { get; set; }
    }

    public class SquadDto
    {
        public string Type { get; set; }
        public int Size { get; set; }
    }

    public class CommodityDto
    {
        public int Amount { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public int Buy { get; set; }
        public int Sell { get; set; }
    }

    public class Coordinates
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    //public static class StarbaseViewModelMapper
    //{
    //    public static Update
    //}
}
