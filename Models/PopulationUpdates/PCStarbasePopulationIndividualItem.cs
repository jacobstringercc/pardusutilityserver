﻿using System.ComponentModel.DataAnnotations;

namespace PardusTracker.Models
{
    public class PCStarbasePopulationIndividualItem
    {
        public int ID { get; set; }
        public int PCStarbasePopulationUpdateID { get; set; }
        public virtual PCStarbasePopulationUpdate PCStarbasePopulationUpdate { get; set; }

        [MaxLength(50)]
        public string StarbaseName { get; set; }
        public int Population { get; set; }
        public Cluster Cluster { get; set; }
    }
}
