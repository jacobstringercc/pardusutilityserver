﻿using System;
using System.Collections.Generic;

namespace PardusTracker.Models
{
    public class PCStarbasePopulationUpdate
    {
        public int ID { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public Universe Universe { get; set; }

        public virtual ICollection<PCStarbasePopulationIndividualItem> Items { get; set; }
    }
}
