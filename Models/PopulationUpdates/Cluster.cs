﻿namespace PardusTracker.Models
{
    public enum Cluster
    {
        PFC, PEC, PUC,
        WPR, NPR, EPR, SPR, 
        LANE, SPLIT, GAP,
        FSH, FHC, FRC,
        UNR, URC, UKC,
        EWS, ESC, EKC
    }
}
