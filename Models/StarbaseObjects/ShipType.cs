﻿using System;
using System.Collections.Generic;

namespace PardusTracker.Models
{
    public enum ShipType
    {
        Sabre,
        Rustclaw,
        Interceptor,
        LannerMini,
        Harrier,
        Mercury,
        Hercules,
        Lanner,
        Hawk,
        Gargantua,
        Behemoth,
        Liberator,
        Leviathan,

        BopX,

        Wasp,
        Adder,
        Thunderbird,
        ViperDc,
        BabelT,
        Piranha,
        Nighthawk,
        NighthawkDeluxe,
        Mantis,
        Extender,
        Gauntlet,
        Doomstar,
        WarNova,

        Ficon,
        Tyrant,
        Spectre,
        ShadowSc,
        Venom,
        Constrictor,
        PhantomAsc,
        Dominator,
        Boa,
        Mooncrusher,

        Rustfire,
        Marauder,
        Junker,
        Slider,
        ElPadre,
        Chitin,
        Horpor,
        Scorpion,

        Rover,
        Reaper,
        BloodLanner,
        SuddenDeath,
        Harvester,

        Trident,
        Celeus,
        Pantagruel,
        Vulcan,
        Nano,
        LiberatorEps
    }

    public static class ShipTypeMapper
    {
        private static Dictionary<string, ShipType> mappings = new Dictionary<string, ShipType>
        {
            { "Lanner Mini", ShipType.LannerMini },
            { "Liberator EPS", ShipType.LiberatorEps },
            { "BOP-X", ShipType.BopX },
            { "Viper Defence Craft", ShipType.ViperDc },
            { "Babel Transporter", ShipType.BabelT },
            { "Nighthawk Deluxe", ShipType.NighthawkDeluxe },
            { "War Nova", ShipType.WarNova },
            { "Shadow SC", ShipType.ShadowSc },
            { "Phantom ASC", ShipType.PhantomAsc },
            { "Boa UC", ShipType.Boa },
            { "Junker IV", ShipType.Junker },
            { "El Padre", ShipType.ElPadre },
            { "Blood Lanner", ShipType.BloodLanner },
            { "Sudden Death", ShipType.SuddenDeath }
        };

        public static ShipType FromString(string value)
        {
            ShipType result;
            if (mappings.TryGetValue(value, out result) || Enum.TryParse(value, out result))
            {
                return result;
            }

            throw new Exception($"{value} is not able to be parsed to ShipType");
        }
    }
}
