﻿using System.Collections.Generic;
using System.Linq;

namespace PardusTracker.Models
{
    public enum StarbaseBuildingLocation
    {
        FirstWest,
        SecondWest,
        ThirdWest,
        FourthWest,
        FirstNorth,
        SecondNorth,
        ThirdNorth,
        FourthNorth, 
        FirstEast,
        SecondEast,
        ThirdEast,
        FourthEast,
        FirstSouth,
        SecondSouth,
        ThirdSouth,
        FourthSouth
    }

    public static class StarbaseBuildingLocationMapper
    {
        private static Dictionary<string, StarbaseBuildingLocation> mappings = new Dictionary<string, StarbaseBuildingLocation>
        {
            { "4,6", StarbaseBuildingLocation.FirstWest},
            { "3,6", StarbaseBuildingLocation.SecondWest},
            { "2,6", StarbaseBuildingLocation.ThirdWest},
            { "1,6", StarbaseBuildingLocation.FourthWest},
            { "6,4", StarbaseBuildingLocation.FirstNorth},
            { "6,3", StarbaseBuildingLocation.SecondNorth},
            { "6,2", StarbaseBuildingLocation.ThirdNorth},
            { "6,1", StarbaseBuildingLocation.FourthNorth},
            { "8,6", StarbaseBuildingLocation.FirstEast},
            { "9,6", StarbaseBuildingLocation.SecondEast},
            { "10,6", StarbaseBuildingLocation.ThirdEast},
            { "11,6", StarbaseBuildingLocation.FourthEast},
            { "6,8", StarbaseBuildingLocation.FirstSouth},
            { "6,9", StarbaseBuildingLocation.SecondSouth},
            { "6,10", StarbaseBuildingLocation.ThirdSouth},
            { "6,11", StarbaseBuildingLocation.FourthSouth},
        };

        public static StarbaseBuildingLocation ToBuildingLocation(string coordinates)
        {
            if (mappings.TryGetValue(coordinates, out StarbaseBuildingLocation value))
            {
                return value;
            }

            throw new System.Exception($"Building location {coordinates} is not supported");
        }

        public static string ToString(StarbaseBuildingLocation location)
        {
            return mappings.Where(entry => entry.Value.Equals(location)).Select(entry => entry.Key).FirstOrDefault();
        }

        public static int NumRingLocation(StarbaseBuildingLocation location) => ((int)location % 4) + 1;
    }
}
