﻿namespace PardusTracker.Models
{
    public class Ship
    {
        public int ID { get; set; }
        public int UpdateID { get; set; }
        public virtual Update Update { get; set; }

        public ShipType Type { get; set; }
        public int Amount { get; set; }
        public int Price { get; set; }
    }
}
