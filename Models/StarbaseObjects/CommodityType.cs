﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PardusTracker.Models
{
    public enum CommodityType
    {
        Food,
        Energy,
        Water,
        Embryos,
        Ore,
        Metal,
        Electronics,
        Robots,
        Plastics,
        Handweapons,
        Battleweapons,
        Droids,
        RadiationCells,
        Medicines,
        Gas,
        Chemicals,
        Gems,
        Optics,
        Liquor,
        Fuel,
        ExoticMatter,
        Biowaste,
        Slaves,
        Drugs,
        NeuralTissue,
        Stims,
        CapriStims,
        CrimsonStims,
        AmberStims,
        Clods,
        Leeches,
        Xparts,
        Drones,
        Stimulators,
        Crystals,
        SapphireJewels,
        RubyJewels,
        BerylJewels,
        FeralEggs
    }

    public static class CommodityTypeMapper
    {
        private static Dictionary<string, CommodityType> mappings = new Dictionary<string, CommodityType>
        {
            { "Animal embryos", CommodityType.Embryos },
            { "Heavy plastics", CommodityType.Plastics },
            { "Hand weapons", CommodityType.Handweapons },
            { "Nebula gas", CommodityType.Gas },
            { "Chemical supplies", CommodityType.Chemicals },
            { "Gem stones", CommodityType.Gems },
            { "Hydrogen fuel", CommodityType.Fuel },
            { "Exotic matter", CommodityType.ExoticMatter },
            { "Optical components", CommodityType.Optics },
            { "Radioactive cells", CommodityType.RadiationCells },
            { "Droid modules", CommodityType.Droids },
            { "Bio-waste", CommodityType.Biowaste },
            { "Nutrient clods", CommodityType.Clods },
            { "Battleweapon Parts", CommodityType.Battleweapons },
            { "Neural Tissue", CommodityType.NeuralTissue },
            { "Stim Chip", CommodityType.Stims },
            { "Capri Stim", CommodityType.CapriStims },
            { "Crimson Stim", CommodityType.CrimsonStims },
            { "Amber Stim", CommodityType.AmberStims }
        };

        public static CommodityType FromString(string value)
        {
            CommodityType result;
            if (mappings.TryGetValue(value, out result) || Enum.TryParse(value, out result))
            {
                return result;
            }

            throw new Exception($"{value} is not able to be parsed to CommodityType");
        }
    }
}
