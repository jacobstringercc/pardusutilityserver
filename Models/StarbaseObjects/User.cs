﻿using System.ComponentModel.DataAnnotations;

namespace PardusTracker.Models
{
    public class User
    {
        public int ID { get; set; }
        public int PardusID { get; set; }
        [MaxLength(30)]
        public string Alias { get; set; }
        public Universe Universe { get; set; }
    }
}
