﻿namespace PardusTracker.Models
{
    public enum Universe
    {
        Orion,
        Artemis,
        Pegasus
    }
}