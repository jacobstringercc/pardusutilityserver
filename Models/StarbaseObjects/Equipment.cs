﻿namespace PardusTracker.Models
{
    public class Equipment
    {
        public int ID { get; set; }
        public int UpdateID { get; set; }
        public virtual Update Update { get; set; }

        public EquipmentType Type { get; set; }
        public int Amount { get; set; }
        public int Price { get; set; }
    }
}
