﻿namespace PardusTracker.Models
{
    public class Squad
    {
        public int ID { get; set; }
        public int UpdateID { get; set; }
        public virtual Update Update { get; set; }

        public int PardusSquadID { get; set; }
        public SquadType Type { get; set; }
        public int Strength { get; set; }
    }

    public enum SquadType
    {
        Bomber,
        Fighter
    }
}
