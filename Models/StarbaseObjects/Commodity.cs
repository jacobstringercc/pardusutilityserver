﻿namespace PardusTracker.Models
{
    public class Commodity
    {
        public int ID { get; set; }
        public int UpdateID { get; set; }
        public virtual Update Update { get; set; }

        public CommodityType Name { get; set; }
        public int Amount { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public int BuyPrice { get; set; }
        public int SellPrice { get; set; }
    }
}
