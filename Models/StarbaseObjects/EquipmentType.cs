﻿using System;
using System.Collections.Generic;

namespace PardusTracker.Models
{
    public enum EquipmentType
    {
        // Guns
        ConvMining10mw,
        ConvMining30mw,
        ConvImpulse1mw,
        ConvImpulse5mw,
        ConvParticle1mw,
        ConvParticle4mw,
        ConvParticle4mwlw,
        ConvParticle20mw,
        ConvParticle20mwlw,
        ConvParticle100mw,
        ConvParticle100mwlw,

        ConvParticle140mw,
        ConvParticle140mwlw,
        ConvGatling4mw,
        ConvGatling4mwlw,
        ConvGatling6mw,
        ConvGatling6mwlw,
        ConvGatling10mw,
        ConvGatling10mwlw,
        ConvGatling20mw,
        ConvGatling20mwlw,
        ConvPlasma35mw,
        ConvPlasma40mw,
        ConvPlasma60mw,

        OrgExoplasm,
        OrgEndoplasm,
        OrgBSA,
        OrgVG,

        Em80mt,
        Em120mt,
        EmLPC,
        EmVLPC,

        LucidiEnforcer1,
        LucidiEnforcer2,
        LucidiConqueror,
        LucidiPlasmaThrower,

        FeralLucidiCrossoverCannon,

        // Missiles
        P80,
        KL760,
        LV111,
        NN500,
        NN550,
        
        MO89,
        MO89Heavy,
        G7,
        A50,
        D70,
        Mk1,
        Mk2,
        Relon,
        OverloadedRelon,
        Kraak,
        Redeemer,

        // Engines
        Nuclear,
        Fusion,
        EnhancedFusion,
        Ion,
        ZIon,
        Antimatter,
        EnhancedAntimatter,
        Hyper,
        ZHyper,
        Interphased,
        EnhancedInterphased,
        FeralSpin,

        // Armours
        ConvArmour1,
        ConvArmour2,
        ConvArmour3,
        ConvArmour4,
        ConvArmour5,
        ConvArmour6,
        OrgArmour1,
        OrgArmour2,
        OrgArmour3,
        OrgArmour4,
        OrgArmour5,
        EmArmour1,
        EmArmour2,
        EmArmour3,
        EmArmour4,
        EmArmour5,
        PardusArmour1,
        PardusArmour2,
        PardusArmour3,
        PardusArmour4,
        PardusArmour5,
        PardusArmour6,

        // Shields
        ShieldTiny,
        ShieldSmall,
        ShieldStandard,
        ShieldMedium,
        ShieldLarge,
        ShieldQSmall,
        ShieldQStandard,
        ShieldQMedium,
        ShieldQLarge,
        ShieldHuge,
        ShieldQHuge,
        ShieldLHuge,
        ShieldLQLarge,
        ShieldLQHuge,

        // Specials
        Autorefueler,
        EscapePod,
        FuelScoop,
        BussardScoop,
        MagneticScoop,
        AmbushTeleporter,
        Class1Teleporter,
        Class2Teleporter,
        MapPack1,
        MapPack2,
        BountyLink1,
        BountyLink2,
        BountyLink3,
        AllianceCombatUplink,
        AllianceTradeUplink,
        GyroStabilizer1,
        GyroStabilizer2,
        BodypastMasker,
        EcmJammer,
        StrongEcmJammer,
        EccmJammer,
        WeakGasFlux,
        WeakEnergyFlux,
        StrongGasFlux,
        StrongEnergyFlux,
        SmallMissileBattery,
        LargeMissileBattery,
        MisappropriatedMissileBattery,
        KeyOfSheppard,
        EpsContrabandScanner,
        EpsContrabandDrone,
        EpsCoreSensitizer,
        CloakingDevice,
        ImprovedCloakingDevice,
        SingleWaveInterferometer,
        DualWaveInterferometer,
        TripleWaveInterferometer,
        SignatureShielding,
        AdvancedSignatureShielding,
        SignatureDissipator,
        ComplexSignatureDissipator,
        DampingFieldGenerator,
        ReinforcedDampingFieldGenerator,
        PerfectedDampingFieldGenerator,
        LightDemat1,
        MediumDemat1,
        HeavyDemat1,
        LightDemat2,
        MediumDemat2,
        HeavyDemat2,
        Hackotron5000,
        Hackotron6000,
        BussardRamscoop2,
        EnhancedMagscoop,
        QuickFreezeModule,
        TraderSafetyShield,
        SpatialWarpPod,
        RemoteAccountingGateway,
        FeralScales
    }

    public static class EquipmentTypeMapper
    {
        private static Dictionary<string, EquipmentType> mappings = new Dictionary<string, EquipmentType>
        {
            { "P80 Sidewinder", EquipmentType.P80 },
            { "KL760 Homing Missile", EquipmentType.KL760 },
            { "LV111 Intelligent Missile", EquipmentType.LV111 },
            { "NN500 Fleet Missile", EquipmentType.NN500 },
            { "NN550 Fleet Missile", EquipmentType.NN550 },
            { "Imperial MO89 Lord Missile", EquipmentType.MO89 },
            { "Imperial G-7 Smartwinder", EquipmentType.G7 },
            { "Imperial A/50 Pogo", EquipmentType.A50 },
            { "Imperial D/70 Havar", EquipmentType.D70 },
            { "Imperial Elite Mk.I", EquipmentType.Mk1 },
            { "Imperial Elite Mk.II", EquipmentType.Mk2 },
            { "King Relon", EquipmentType.Relon },
            { "King Kraak", EquipmentType.Kraak },
            { "Royal Redeemer", EquipmentType.Redeemer },
            { "10 MW Mining Laser", EquipmentType.ConvMining10mw },
            { "30 MW Mining Laser", EquipmentType.ConvMining30mw },
            { "1 MW Impulse Laser", EquipmentType.ConvImpulse1mw },
            { "5 MW Impulse Laser", EquipmentType.ConvImpulse5mw },
            { "1 MW Particle Laser", EquipmentType.ConvParticle1mw },
            { "4 MW Particle Laser", EquipmentType.ConvParticle4mw },
            { "4 MW Light-weight Particle Laser", EquipmentType.ConvParticle4mwlw },
            { "20 MW Particle Laser", EquipmentType.ConvParticle20mw },
            { "20 MW Light-weight Particle Laser", EquipmentType.ConvParticle20mwlw },
            { "100 MW Particle Laser", EquipmentType.ConvParticle100mw },
            { "100 MW Light-weight Particle Laser", EquipmentType.ConvParticle100mwlw },
            { "140 MW Particle Laser", EquipmentType.ConvParticle140mw },
            { "140 MW Light-weight Particle Laser", EquipmentType.ConvParticle140mwlw },
            { "4 MW Gatling Laser", EquipmentType.ConvGatling4mw },
            { "4 MW Light-weight Gatling Laser", EquipmentType.ConvGatling4mwlw },
            { "6 MW Gatling Laser", EquipmentType.ConvGatling6mw },
            { "6 MW Light-weight Gatling Laser", EquipmentType.ConvGatling6mwlw },
            { "10 MW Gatling Laser", EquipmentType.ConvGatling10mw },
            { "10 MW Light-weight Gatling Laser", EquipmentType.ConvGatling10mwlw },
            { "35 MW Plasma Laser", EquipmentType.ConvPlasma35mw },
            { "40 MW Plasma Laser", EquipmentType.ConvPlasma40mw },
            { "60 MW Plasma Laser", EquipmentType.ConvPlasma60mw },
            { "20 MW Gatling", EquipmentType.ConvGatling20mw },
            { "20 MW Light-weight Gatling", EquipmentType.ConvGatling20mwlw },
            { "Nuclear Drive", EquipmentType.Nuclear },
            { "Fusion Drive", EquipmentType.Fusion },
            { "Ion Drive", EquipmentType.Ion },
            { "Anti-Matter Drive", EquipmentType.Antimatter },
            { "Hyper Drive", EquipmentType.Hyper },
            { "Interphased Drive", EquipmentType.Interphased },
            { "Enhanced Fusion Drive", EquipmentType.EnhancedFusion },
            { "Enhanced Anti-Matter Drive", EquipmentType.EnhancedAntimatter },
            { "Enhanced Interphased Drive", EquipmentType.EnhancedInterphased },
            { "Titanium Armor", EquipmentType.ConvArmour1 },
            { "Tritanium Armor", EquipmentType.ConvArmour2 },
            { "Zortrium Armor", EquipmentType.ConvArmour3 },
            { "Neutronium Armor", EquipmentType.ConvArmour4 },
            { "Adamantium Armor", EquipmentType.ConvArmour5 },
            { "Ebidium Armor", EquipmentType.ConvArmour6 },
            { "Small Shield Generator", EquipmentType.ShieldSmall },
            { "Standard Shield Generator", EquipmentType.ShieldStandard },
            { "Medium Shield Generator", EquipmentType.ShieldMedium },
            { "Large Shield Generator", EquipmentType.ShieldLarge },
            // Union shields
            { "Fuel Scoop", EquipmentType.FuelScoop },
            { "Auto Refueler", EquipmentType.Autorefueler },
            { "Escape Pod", EquipmentType.EscapePod },
            { "Magnetic Scoop", EquipmentType.MagneticScoop },
            { "Enhanced Magnetic Scoop", EquipmentType.EnhancedMagscoop },
            { "Bussard Ramscoop", EquipmentType.BussardScoop },
            { "Bussard Ramscoop 2.0", EquipmentType.BussardRamscoop2 },
            { "Class I Teleporter", EquipmentType.Class1Teleporter },
            { "Class II Teleporter", EquipmentType.Class2Teleporter },
            { "Ambush Teleporter", EquipmentType.AmbushTeleporter },
            { "ECM Jammer", EquipmentType.EcmJammer },
            { "Software Upgrade - Map Pack I", EquipmentType.MapPack1 },
            { "Software Upgrade - Map Pack II", EquipmentType.MapPack2 },
            { "Software Upgrade - Bounty Link I", EquipmentType.BountyLink1 },
            { "Software Upgrade - Bounty Link II", EquipmentType.BountyLink2 },
            { "Software Upgrade - Bounty Link III", EquipmentType.BountyLink3 },
            { "Software Upgrade - Alliance Combat Uplink", EquipmentType.AllianceCombatUplink },
            { "Software Upgrade - Alliance Trade Uplink", EquipmentType.AllianceTradeUplink },
            { "EPS Contraband Scanner", EquipmentType.EpsContrabandScanner },
            { "EPS Contraband Drone", EquipmentType.EpsContrabandDrone },
            { "EPS Core Sensitizer", EquipmentType.EpsCoreSensitizer },
            // Union specials
            // Planet specials
            //{ "", EquipmentType.BloodLanner },
            //{ "", EquipmentType.SuddenDeath },
            //{ "", EquipmentType.Boa },
            //{ "", EquipmentType.Junker },
            //{ "", EquipmentType.ElPadre },
            //{ "", EquipmentType.BloodLanner },
            //{ "", EquipmentType.SuddenDeath },
            //{ "", EquipmentType.WarNova },
            //{ "", EquipmentType.ShadowSc },
            //{ "", EquipmentType.PhantomAsc },
            //{ "", EquipmentType.Boa },
            //{ "", EquipmentType.Junker },
            //{ "", EquipmentType.ElPadre },
            //{ "", EquipmentType.BloodLanner },
            //{ "", EquipmentType.SuddenDeath },
            //{ "", EquipmentType.WarNova },
            //{ "", EquipmentType.ShadowSc },
            //{ "", EquipmentType.PhantomAsc },
            //{ "", EquipmentType.Boa },
            //{ "", EquipmentType.Junker },
            //{ "", EquipmentType.ElPadre },
            //{ "", EquipmentType.BloodLanner },
            //{ "", EquipmentType.SuddenDeath }
        };

        public static EquipmentType FromString(string value)
        {
            value = value.Replace("RANK\n \t", "");

            EquipmentType result;
            if (mappings.TryGetValue(value, out result) || Enum.TryParse(value, out result))
            {
                return result;
            }

            throw new Exception($"{value} is not able to be parsed to EquipmentType");
        }
    }
}
