﻿using System.Collections.Generic;
using System.Linq;

namespace PardusTracker.Models
{
    public enum StarbaseBuildingType
    {
        Warehouse,
        RepairBay,
        ShortRangeScanner,
        LightDefenseArtillery,
        MediumDefenseArtillery,
        HeavyDefenseArtillery,
        WeaponFactory,
        SpecialFactory,
        EngineFactory,
        ArmourFactory,
        ShieldFactory,
        SmallShipyard,
        MediumShipyard,
        LargeShipyard,
        Empty,
        NotAvailable
    }

    public static class StarbaseBuildingTypeMapper
    {
        private static Dictionary<string, StarbaseBuildingType> mappings = new Dictionary<string, StarbaseBuildingType>
        {
            { "Warehouse", StarbaseBuildingType.Warehouse},
            { "Repair", StarbaseBuildingType.RepairBay},
            { "SRS", StarbaseBuildingType.ShortRangeScanner},
            { "LDA", StarbaseBuildingType.LightDefenseArtillery},
            { "SDA", StarbaseBuildingType.MediumDefenseArtillery},
            { "HDA", StarbaseBuildingType.HeavyDefenseArtillery},
            { "Weapon", StarbaseBuildingType.WeaponFactory},
            { "Special", StarbaseBuildingType.SpecialFactory},
            { "Drive", StarbaseBuildingType.EngineFactory},
            { "Armor", StarbaseBuildingType.ArmourFactory},
            { "Shield", StarbaseBuildingType.ShieldFactory},
            { "Sml Ship", StarbaseBuildingType.SmallShipyard},
            { "Med Ship", StarbaseBuildingType.MediumShipyard},
            { "Huge Ship", StarbaseBuildingType.LargeShipyard},
            { "Empty", StarbaseBuildingType.Empty},
            { "-", StarbaseBuildingType.NotAvailable},
        };

        public static StarbaseBuildingType ToBuildingType(string building)
        {
            if (mappings.TryGetValue(building, out StarbaseBuildingType value))
            {
                return value;
            }

            throw new System.Exception($"Building type {building} is not supported");
        }

        public static string ToString(StarbaseBuildingType type)
        {
            return mappings.Where(entry => entry.Value.Equals(type)).Select(entry => entry.Key).FirstOrDefault();
        }
    }
}
