﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PardusTracker.Models
{
    public class Starbase
    {
        public int ID { get; set; }
        public Universe Universe { get; set; }
        [MaxLength(30)]
        public string Sector { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        [MaxLength(50)]
        public string LatestStarbaseName { get; set; }
        public int? MinimumPop { get; set; }
        public int? MaximumPop { get; set; }
        public DateTimeOffset MinimumPopEdited { get; set; }

        public virtual ICollection<Update> Updates { get; set; }
        public bool IsActive { get; set; }
    }

    public class Update
    {
        public int ID { get; set; }
        [MaxLength(50)]
        public string StarbaseName { get; set; }
        [MaxLength(30)]
        public string Owner { get; set; }
        public DateTimeOffset UpdateTime { get; set; }
        public int? Population { get; set; }
        public bool? PopulationIsExact { get; set; }

        public int? Credits { get; set; }
        public int? FreeSpace { get; set; }

        public int StarbaseID { get; set; }
        public virtual Starbase Starbase { get; set; }

        public int UserID { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<StarbaseBuilding> Buildings { get; set; }
        public virtual ICollection<Commodity> Commodities { get; set; }
        public virtual ICollection<Ship> Ships { get; set; }
        public virtual ICollection<Equipment> Equipment { get; set; }
        public virtual ICollection<Squad> Squads { get; set; }
    }
}
