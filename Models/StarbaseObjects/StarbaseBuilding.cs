﻿namespace PardusTracker.Models
{
    public class StarbaseBuilding
    {
        public int ID { get; set; }
        public int UpdateID { get; set; }
        public virtual Update Update { get; set; }

        public StarbaseBuildingLocation Location { get; set; }
        public StarbaseBuildingType Type { get; set; }
    }
}
