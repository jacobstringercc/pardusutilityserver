﻿using System;
using System.Collections.Generic;

namespace PardusTracker.Models
{
    public class PCStarbasePopulationUpdateDto
    {
        public Dictionary<string, int[]> PEC;
        public Dictionary<string, int[]> PFC;
        public Dictionary<string, int[]> PUC;
    }
}
