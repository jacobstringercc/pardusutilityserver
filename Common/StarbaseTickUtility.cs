﻿using System;

namespace Common
{
    public static class StarbaseTickUtility
    {
        private static DateTimeOffset[] _starbaseTicks =
        {
            new DateTimeOffset(1900, 1, 1, 0, 25, 0, TimeSpan.Zero),
            new DateTimeOffset(1900, 1, 1, 3, 25, 0, TimeSpan.Zero),
            new DateTimeOffset(1900, 1, 1, 6, 25, 0, TimeSpan.Zero),
            new DateTimeOffset(1900, 1, 1, 9, 25, 0, TimeSpan.Zero),
            new DateTimeOffset(1900, 1, 1, 12, 25, 0, TimeSpan.Zero),
            new DateTimeOffset(1900, 1, 1, 15, 25, 0, TimeSpan.Zero),
            new DateTimeOffset(1900, 1, 1, 18, 25, 0, TimeSpan.Zero),
            new DateTimeOffset(1900, 1, 1, 21, 25, 0, TimeSpan.Zero)
        };

        public static bool isSameStarbaseTick(this DateTimeOffset first, DateTimeOffset second)
        {
            var difference = first - second;

            if (difference.Duration() < TimeSpan.FromHours(3)) // Within 3 hours, might be in the same tick (keeps within the same 24 hour period, meaning we don't need to check dates)
            {
                return nextTick(first) == nextTick(second);
            }

            return false;
        }

        public static bool isNextStarbaseTick(this DateTimeOffset first, DateTimeOffset second)
        {
            var difference = first - second;

            if (difference.Duration() < TimeSpan.FromHours(6)) // Within 6 hours, might be in the same tick (keeps within the same 24 hour period, meaning we don't need to check dates)
            {
                int positionDifference = Math.Abs(nextTick(first) - nextTick(second));
                return positionDifference == 1 || positionDifference == 7; // We don't care what order we got the dates in. 7 will happen crossing over date lines.
            }

            return false;
        }

        private static int nextTick(DateTimeOffset date)
        {
            for (var i = 0; i < _starbaseTicks.Length; i++)
            {
                if (date.Hour < _starbaseTicks[i].Hour || date.Hour == _starbaseTicks[i].Hour && date.Minute < _starbaseTicks[i].Minute)
                {
                    return i;
                }
            }

            return 0;
        }
    }
}
